const Agenda = require('agenda');
const { start, serverIsStarted } = require('./server');
const { CONNECTION, TIME_INTERVAL, PROCESS_EVERY } = require('./config.js');
const { syncShopifyInfo } = require('./queuing');
let { run_count } = require('./config.js');

const syncShopifyInventory = 'sync shopify inventory';

(async () => {
  await start();
})();

let startTimer = new Date();

console.log(new Date() - startTimer);

// Create agenda
const agenda = new Agenda({
  db: {
    address: CONNECTION,
    collection: 'variantmodels',
  },
  processEvery: PROCESS_EVERY + ' seconds', // how often agenda checks for tasks to be run
});

agenda.defaultLockLifetime(1000);

// connect then define the task to be run
agenda.define(syncShopifyInventory, {
  priority: 'high',
  concurrency: 10,
}, async () => {
  await syncShopifyInfo(); // puts task on the queue
  console.log('run', run_count++); // test with delay and call sync again after adding something
});

// Sync shopify inventory every timeInterval
(async () => {
  try {
    await agenda.start();
    await agenda.every(TIME_INTERVAL + ' seconds', syncShopifyInventory, {}, {}); // need all 4 params or does not work
  } catch (error) {
    return error;
  }
})();
