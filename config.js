/* eslint-disable no-unused-vars */
// eslint-disable-next-line one-var

const Hapi = require('hapi'),
  CONNECTION = 'mongodb://localhost/kellivariantmodel',
  SERVER = new Hapi.Server({
    host: 'localhost',
    port: 3000,
  }),
  TIME_INTERVAL = 1,
  PROCESS_EVERY = 3,
  GET = 'GET',
  POST = 'POST',
  PUSH = 'PUSH',
  DELETE = 'DELETE';

// eslint-disable-next-line one-var
const apiKey = 'd59e3497cee3a716dfaa20c07bb26e20',
  storefront_access_token = '4f3299985f0fd340d8c8401e822e26c4',
  apiPassword = 'c1d9aedbb2389284759c4040c42786c9',
  shopifyHostname = 'kelli-franks-store.myshopify.com',
  apiVersion = '2019-07',
  REQUEST_SHOPIFY_BASE_URL = `https://${apiKey}:${apiPassword}@${shopifyHostname}/admin/api/${apiVersion}/`;

let mongodbId,
  run_count = 0;


const sampleAllVariants =
  {
    'products': [
      {
        'id': mongodbId,
        'title': '32oz Water Bottle Bundle',
        'body_html': 'Bundle of all colors of the 32oz water bottle',
        'vendor': 'Kelli Frank\'s Store',
        'product_type': '',
        'created_at': '2019-07-26T17:32:33-04:00',
        'handle': '32oz-water-bottle-bundle',
        'updated_at': '2019-07-26T17:32:36-04:00',
        'published_at': '2019-07-26T17:30:23-04:00',
        'template_suffix': null,
        'tags': '',
        'published_scope': 'web',
        'admin_graphql_api_id': 'gid:\/\/shopify\/Product\/3827836256316',
        'variants': [
          {
            'id': 29348288593980,
            'product_id': 3827836256316,
            'title': 'Default Title',
            'price': '0.00',
            'sku': '',
            'position': 1,
            'inventory_policy': 'deny',
            'compare_at_price': null,
            'fulfillment_service': 'manual',
            'inventory_management': 'shopify',
            'option1': 'Default Title',
            'option2': null,
            'option3': null,
            'created_at': '2019-07-26T17:32:34-04:00',
            'updated_at': '2019-07-26T17:32:34-04:00',
            'taxable': true,
            'barcode': '',
            'grams': 0,
            'image_id': null,
            'weight': 0.0,
            'weight_unit': 'lb',
            'inventory_item_id': 30457597263932,
            'inventory_quantity': 0,
            'old_inventory_quantity': 0,
            'requires_shipping': true,
            'admin_graphql_api_id': 'gid:\/\/shopify\/ProductVariant\/29348288593980'
          }
        ],
        'options': [
          {
            'id': 5026630893628,
            'product_id': 3827836256316,
            'name': 'Title',
            'position': 1,
            'values': [
              'Default Title'
            ]
          }
        ],
        'images': [
          {
            'id': 11915772624956,
            'product_id': 3827836256316,
            'position': 1,
            'created_at': '2019-07-26T17:32:36-04:00',
            'updated_at': '2019-07-26T17:32:36-04:00',
            'alt': null,
            'width': 245,
            'height': 206,
            'src': 'https:\/\/cdn.shopify.com\/s\/files\/1\/0259\/0670\/8540\/products\/images.jpeg?v=1564176756',
            'variant_ids': [],
            'admin_graphql_api_id': 'gid:\/\/shopify\/ProductImage\/11915772624956'
          }
        ],
        'image': {
          'id': 11915772624956,
          'product_id': 3827836256316,
          'position': 1,
          'created_at': '2019-07-26T17:32:36-04:00',
          'updated_at': '2019-07-26T17:32:36-04:00',
          'alt': null,
          'width': 245,
          'height': 206,
          'src': 'https:\/\/cdn.shopify.com\/s\/files\/1\/0259\/0670\/8540\/products\/images.jpeg?v=1564176756',
          'variant_ids': [],
          'admin_graphql_api_id': 'gid:\/\/shopify\/ProductImage\/11915772624956'
        }
      },
      {
        'id': 3813097275452,
        'title': 'First Product',
        'body_html': '',
        'vendor': 'Kelli Frank\'s Store',
        'product_type': '',
        'created_at': '2019-07-22T19:59:32-04:00',
        'handle': 'first-product',
        'updated_at': '2019-07-26T13:14:37-04:00',
        'published_at': '2019-07-22T19:58:49-04:00',
        'template_suffix': null,
        'tags': '',
        'published_scope': 'web',
        'admin_graphql_api_id': 'gid:\/\/shopify\/Product\/3813097275452',
        'variants': [
          {
            'id': 29293785907260,
            'product_id': 3813097275452,
            'title': 'Default Title',
            'price': '0.00',
            'sku': 'asdf',
            'position': 1,
            'inventory_policy': 'deny',
            'compare_at_price': null,
            'fulfillment_service': 'manual',
            'inventory_management': 'shopify',
            'option1': 'Default Title',
            'option2': null,
            'option3': null,
            'created_at': '2019-07-22T19:59:32-04:00',
            'updated_at': '2019-07-26T13:14:37-04:00',
            'taxable': true,
            'barcode': '',
            'grams': 0,
            'image_id': null,
            'weight': 0.0,
            'weight_unit': 'lb',
            'inventory_item_id': 30398140350524,
            'inventory_quantity': 0,
            'old_inventory_quantity': 0,
            'requires_shipping': true,
            'admin_graphql_api_id': 'gid:\/\/shopify\/ProductVariant\/29293785907260'
          }
        ],
        'options': [
          {
            'id': 5009236033596,
            'product_id': 3813097275452,
            'name': 'Title',
            'position': 1,
            'values': [
              'Default Title'
            ]
          }
        ],
        'images': [],
        'image': null
      },
      {
        'id': 3816147877948,
        'title': 'Mug',
        'body_html': 'White coffee mug',
        'vendor': 'Kelli Frank\'s Store',
        'product_type': '',
        'created_at': '2019-07-23T16:03:36-04:00',
        'handle': 'mug',
        'updated_at': '2019-07-25T12:55:04-04:00',
        'published_at': '2019-07-23T16:01:32-04:00',
        'template_suffix': null,
        'tags': '',
        'published_scope': 'web',
        'admin_graphql_api_id': 'gid:\/\/shopify\/Product\/3816147877948',
        'variants': [
          {
            'id': 29306257604668,
            'product_id': 3816147877948,
            'title': 'Default Title',
            'price': '2.00',
            'sku': 'MG-1',
            'position': 1,
            'inventory_policy': 'deny',
            'compare_at_price': null,
            'fulfillment_service': 'manual',
            'inventory_management': 'shopify',
            'option1': 'Default Title',
            'option2': null,
            'option3': null,
            'created_at': '2019-07-23T16:03:36-04:00',
            'updated_at': '2019-07-24T12:56:19-04:00',
            'taxable': true,
            'barcode': '',
            'grams': 0,
            'image_id': null,
            'weight': 0.0,
            'weight_unit': 'lb',
            'inventory_item_id': 30411711447100,
            'inventory_quantity': 6,
            'old_inventory_quantity': 6,
            'requires_shipping': true,
            'admin_graphql_api_id': 'gid:\/\/shopify\/ProductVariant\/29306257604668'
          }
        ],
        'options': [
          {
            'id': 5013047541820,
            'product_id': 3816147877948,
            'name': 'Title',
            'position': 1,
            'values': [
              'Default Title'
            ]
          }
        ],
        'images': [
          {
            'id': 11886520008764,
            'product_id': 3816147877948,
            'position': 1,
            'created_at': '2019-07-23T16:04:07-04:00',
            'updated_at': '2019-07-23T16:04:07-04:00',
            'alt': null,
            'width': 225,
            'height': 225,
            'src': 'https:\/\/cdn.shopify.com\/s\/files\/1\/0259\/0670\/8540\/products\/download-1.jpg?v=1563912247',
            'variant_ids': [],
            'admin_graphql_api_id': 'gid:\/\/shopify\/ProductImage\/11886520008764'
          }
        ],
        'image': {
          'id': 11886520008764,
          'product_id': 3816147877948,
          'position': 1,
          'created_at': '2019-07-23T16:04:07-04:00',
          'updated_at': '2019-07-23T16:04:07-04:00',
          'alt': null,
          'width': 225,
          'height': 225,
          'src': 'https:\/\/cdn.shopify.com\/s\/files\/1\/0259\/0670\/8540\/products\/download-1.jpg?v=1563912247',
          'variant_ids': [],
          'admin_graphql_api_id': 'gid:\/\/shopify\/ProductImage\/11886520008764'
        }
      },
      {
        'id': 3816136015932,
        'title': 'Water Bottle',
        'body_html': 'water bottle',
        'vendor': 'Kelli Frank\'s Store',
        'product_type': '',
        'created_at': '2019-07-23T15:56:35-04:00',
        'handle': 'water-bottle',
        'updated_at': '2019-07-25T12:55:04-04:00',
        'published_at': '2019-07-23T15:55:33-04:00',
        'template_suffix': null,
        'tags': '',
        'published_scope': 'web',
        'admin_graphql_api_id': 'gid:\/\/shopify\/Product\/3816136015932',
        'variants': [
          {
            'id': 29306212745276,
            'product_id': 3816136015932,
            'title': '32oz \/ blue',
            'price': '13.00',
            'sku': 'WB-1',
            'position': 1,
            'inventory_policy': 'deny',
            'compare_at_price': null,
            'fulfillment_service': 'manual',
            'inventory_management': 'shopify',
            'option1': '32oz',
            'option2': 'blue',
            'option3': null,
            'created_at': '2019-07-23T15:56:35-04:00',
            'updated_at': '2019-07-23T16:01:20-04:00',
            'taxable': true,
            'barcode': '',
            'grams': 0,
            'image_id': null,
            'weight': 0.0,
            'weight_unit': 'lb',
            'inventory_item_id': 30411660492860,
            'inventory_quantity': 0,
            'old_inventory_quantity': 0,
            'requires_shipping': true,
            'admin_graphql_api_id': 'gid:\/\/shopify\/ProductVariant\/29306212745276'
          },
          {
            'id': 29306240827452,
            'product_id': 3816136015932,
            'title': '32oz \/ green',
            'price': '13.00',
            'sku': 'WB-2',
            'position': 2,
            'inventory_policy': 'deny',
            'compare_at_price': null,
            'fulfillment_service': 'manual',
            'inventory_management': 'shopify',
            'option1': '32oz',
            'option2': 'green',
            'option3': null,
            'created_at': '2019-07-23T16:01:20-04:00',
            'updated_at': '2019-07-23T16:01:20-04:00',
            'taxable': true,
            'barcode': '',
            'grams': 0,
            'image_id': null,
            'weight': 0.0,
            'weight_unit': 'lb',
            'inventory_item_id': 30411692048444,
            'inventory_quantity': 0,
            'old_inventory_quantity': 0,
            'requires_shipping': true,
            'admin_graphql_api_id': 'gid:\/\/shopify\/ProductVariant\/29306240827452'
          },
          {
            'id': 29306240860220,
            'product_id': 3816136015932,
            'title': '32oz \/ purple',
            'price': '13.00',
            'sku': 'WB-3',
            'position': 3,
            'inventory_policy': 'deny',
            'compare_at_price': null,
            'fulfillment_service': 'manual',
            'inventory_management': 'shopify',
            'option1': '32oz',
            'option2': 'purple',
            'option3': null,
            'created_at': '2019-07-23T16:01:20-04:00',
            'updated_at': '2019-07-23T16:01:20-04:00',
            'taxable': true,
            'barcode': '',
            'grams': 0,
            'image_id': null,
            'weight': 0.0,
            'weight_unit': 'lb',
            'inventory_item_id': 30411692081212,
            'inventory_quantity': 0,
            'old_inventory_quantity': 0,
            'requires_shipping': true,
            'admin_graphql_api_id': 'gid:\/\/shopify\/ProductVariant\/29306240860220'
          },
          {
            'id': 29306240892988,
            'product_id': 3816136015932,
            'title': '24oz \/ blue',
            'price': '13.00',
            'sku': 'WB-4',
            'position': 4,
            'inventory_policy': 'deny',
            'compare_at_price': null,
            'fulfillment_service': 'manual',
            'inventory_management': 'shopify',
            'option1': '24oz',
            'option2': 'blue',
            'option3': null,
            'created_at': '2019-07-23T16:01:20-04:00',
            'updated_at': '2019-07-23T16:01:20-04:00',
            'taxable': true,
            'barcode': '',
            'grams': 0,
            'image_id': null,
            'weight': 0.0,
            'weight_unit': 'lb',
            'inventory_item_id': 30411692113980,
            'inventory_quantity': 0,
            'old_inventory_quantity': 0,
            'requires_shipping': true,
            'admin_graphql_api_id': 'gid:\/\/shopify\/ProductVariant\/29306240892988'
          },
          {
            'id': 29306240925756,
            'product_id': 3816136015932,
            'title': '24oz \/ green',
            'price': '13.00',
            'sku': 'WB-5',
            'position': 5,
            'inventory_policy': 'deny',
            'compare_at_price': null,
            'fulfillment_service': 'manual',
            'inventory_management': 'shopify',
            'option1': '24oz',
            'option2': 'green',
            'option3': null,
            'created_at': '2019-07-23T16:01:20-04:00',
            'updated_at': '2019-07-23T20:40:10-04:00',
            'taxable': true,
            'barcode': '',
            'grams': 0,
            'image_id': null,
            'weight': 0.0,
            'weight_unit': 'lb',
            'inventory_item_id': 30411692146748,
            'inventory_quantity': 0,
            'old_inventory_quantity': 0,
            'requires_shipping': true,
            'admin_graphql_api_id': 'gid:\/\/shopify\/ProductVariant\/29306240925756'
          },
          {
            'id': 29306240958524,
            'product_id': 3816136015932,
            'title': '24oz \/ purple',
            'price': '13.00',
            'sku': 'WB-6',
            'position': 6,
            'inventory_policy': 'deny',
            'compare_at_price': null,
            'fulfillment_service': 'manual',
            'inventory_management': 'shopify',
            'option1': '24oz',
            'option2': 'purple',
            'option3': null,
            'created_at': '2019-07-23T16:01:20-04:00',
            'updated_at': '2019-07-23T16:01:20-04:00',
            'taxable': true,
            'barcode': '',
            'grams': 0,
            'image_id': null,
            'weight': 0.0,
            'weight_unit': 'lb',
            'inventory_item_id': 30411692179516,
            'inventory_quantity': 0,
            'old_inventory_quantity': 0,
            'requires_shipping': true,
            'admin_graphql_api_id': 'gid:\/\/shopify\/ProductVariant\/29306240958524'
          },
          {
            'id': 29306240991292,
            'product_id': 3816136015932,
            'title': '16oz \/ blue',
            'price': '13.00',
            'sku': 'WB-7',
            'position': 7,
            'inventory_policy': 'deny',
            'compare_at_price': null,
            'fulfillment_service': 'manual',
            'inventory_management': 'shopify',
            'option1': '16oz',
            'option2': 'blue',
            'option3': null,
            'created_at': '2019-07-23T16:01:20-04:00',
            'updated_at': '2019-07-23T16:01:20-04:00',
            'taxable': true,
            'barcode': '',
            'grams': 0,
            'image_id': null,
            'weight': 0.0,
            'weight_unit': 'lb',
            'inventory_item_id': 30411692212284,
            'inventory_quantity': 0,
            'old_inventory_quantity': 0,
            'requires_shipping': true,
            'admin_graphql_api_id': 'gid:\/\/shopify\/ProductVariant\/29306240991292'
          },
          {
            'id': 29306241024060,
            'product_id': 3816136015932,
            'title': '16oz \/ green',
            'price': '13.00',
            'sku': 'WB-8',
            'position': 8,
            'inventory_policy': 'deny',
            'compare_at_price': null,
            'fulfillment_service': 'manual',
            'inventory_management': 'shopify',
            'option1': '16oz',
            'option2': 'green',
            'option3': null,
            'created_at': '2019-07-23T16:01:20-04:00',
            'updated_at': '2019-07-23T16:01:20-04:00',
            'taxable': true,
            'barcode': '',
            'grams': 0,
            'image_id': null,
            'weight': 0.0,
            'weight_unit': 'lb',
            'inventory_item_id': 30411692245052,
            'inventory_quantity': 0,
            'old_inventory_quantity': 0,
            'requires_shipping': true,
            'admin_graphql_api_id': 'gid:\/\/shopify\/ProductVariant\/29306241024060'
          },
          {
            'id': 29306241056828,
            'product_id': 3816136015932,
            'title': '16oz \/ purple',
            'price': '13.00',
            'sku': 'WB-9',
            'position': 9,
            'inventory_policy': 'deny',
            'compare_at_price': null,
            'fulfillment_service': 'manual',
            'inventory_management': 'shopify',
            'option1': '16oz',
            'option2': 'purple',
            'option3': null,
            'created_at': '2019-07-23T16:01:20-04:00',
            'updated_at': '2019-07-23T20:40:11-04:00',
            'taxable': true,
            'barcode': '',
            'grams': 0,
            'image_id': null,
            'weight': 0.0,
            'weight_unit': 'lb',
            'inventory_item_id': 30411692277820,
            'inventory_quantity': 0,
            'old_inventory_quantity': 0,
            'requires_shipping': true,
            'admin_graphql_api_id': 'gid:\/\/shopify\/ProductVariant\/29306241056828'
          }
        ],
        'options': [
          {
            'id': 5013034369084,
            'product_id': 3816136015932,
            'name': 'Size',
            'position': 1,
            'values': [
              '32oz',
              '24oz',
              '16oz'
            ]
          },
          {
            'id': 5013042954300,
            'product_id': 3816136015932,
            'name': 'Color',
            'position': 2,
            'values': [
              'blue',
              'green',
              'purple'
            ]
          }
        ],
        'images': [
          {
            'id': 11886497071164,
            'product_id': 3816136015932,
            'position': 1,
            'created_at': '2019-07-23T15:56:37-04:00',
            'updated_at': '2019-07-23T15:56:37-04:00',
            'alt': null,
            'width': 225,
            'height': 225,
            'src': 'https:\/\/cdn.shopify.com\/s\/files\/1\/0259\/0670\/8540\/products\/download.jpg?v=1563911797',
            'variant_ids': [],
            'admin_graphql_api_id': 'gid:\/\/shopify\/ProductImage\/11886497071164'
          }
        ],
        'image': {
          'id': 11886497071164,
          'product_id': 3816136015932,
          'position': 1,
          'created_at': '2019-07-23T15:56:37-04:00',
          'updated_at': '2019-07-23T15:56:37-04:00',
          'alt': null,
          'width': 225,
          'height': 225,
          'src': 'https:\/\/cdn.shopify.com\/s\/files\/1\/0259\/0670\/8540\/products\/download.jpg?v=1563911797',
          'variant_ids': [],
          'admin_graphql_api_id': 'gid:\/\/shopify\/ProductImage\/11886497071164'
        }
      }
    ]
  };

const sampleAllVariantsStripped =
  [
    {
      'shopifyId': 29348288593980,
      'title': '32oz Water Bottle Bundle',
      'sku': '',
      'stock': 0,
      'source': '???'
    },
    {
      'shopifyId': 29293785907260,
      'title': 'First Product',
      'sku': 'asdf',
      'stock': 0,
      'source': '???'
    },
    {
      'shopifyId': 29306257604668,
      'title': 'Mug',
      'sku': 'MG-1',
      'stock': 6,
      'source': '???'
    },
    {
      'shopifyId': 29306212745276,
      'title': 'Water Bottle',
      'sku': 'WB-1',
      'stock': 0,
      'source': '???'
    },
    {
      'shopifyId': 29306240827452,
      'title': 'Water Bottle',
      'sku': 'WB-2',
      'stock': 0,
      'source': '???'
    },
    {
      'shopifyId': 29306240860220,
      'title': 'Water Bottle',
      'sku': 'WB-3',
      'stock': 0,
      'source': '???'
    },
    {
      'shopifyId': 29306240892988,
      'title': 'Water Bottle',
      'sku': 'WB-4',
      'stock': 0,
      'source': '???'
    },
    {
      'shopifyId': 29306240925756,
      'title': 'Water Bottle',
      'sku': 'WB-5',
      'stock': 0,
      'source': '???'
    },
    {
      'shopifyId': 29306240958524,
      'title': 'Water Bottle',
      'sku': 'WB-6',
      'stock': 0,
      'source': '???'
    },
    {
      'shopifyId': 29306240991292,
      'title': 'Water Bottle',
      'sku': 'WB-7',
      'stock': 0,
      'source': '???'
    },
    {
      'shopifyId': 29306241024060,
      'title': 'Water Bottle',
      'sku': 'WB-8',
      'stock': 0,
      'source': '???'
    },
    {
      'shopifyId': 29306241056828,
      'title': 'Water Bottle',
      'sku': 'WB-9',
      'stock': 0,
      'source': '???'
    }
  ];

const mongoose = require('mongoose');

const VariantModel = mongoose.model('VariantModel', {
  shopifyId: String,
  title: String,
  sku: String,
  stock: Number,
  source: String,
});

const testPOSTBodyNo_id = {
  title: 'testing title',
  sku: 'test sku',
  stock: 0,
  shopifyId: '123',
  source: 'test source'
};

const bodyWith_id = {
  _id: mongodbId,
  title: 'explicit _id test title',
  sku: 'explicit _id test sku',
  stock: -1,
  shopifyId: '123',
  source: 'explicit _id test source'
};


module.exports = {
  CONNECTION,
  SERVER,
  TIME_INTERVAL,
  PROCESS_EVERY,
  GET,
  POST,
  DELETE,
  REQUEST_SHOPIFY_BASE_URL,
  sampleAllVariants,
  sampleAllVariantsStripped,
  VariantModel,
  shopifyHostname,
  testPOSTBodyNo_id,
  bodyWith_id,
  run_count
};
