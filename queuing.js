const rp = require('request-promise');
const Joi = require('joi');
const mongoose = require('mongoose');
const kue = require('kue'); // need for server to connect
const {
  VariantModel, REQUEST_SHOPIFY_BASE_URL, CONNECTION, SERVER, GET, POST, sampleAllVariants, DELETE
} = require('./config.js');

require('./routes.js'); // need for routes to run

mongoose.connect(CONNECTION, { useNewUrlParser: true }, (err) => {
  if (err) {
    console.log(`error: ${err}`);
  }
});

// Generic request
async function makeRequest(method, url) {
  return rp({
    method,
    uri: url,
    json: true, // returns as json object instead of string representation
  });
}


// gets all products from shpopify and updates them in mongodb
async function getAllProducts() { // tested parsing
  try {
    const allProducts = await makeRequest(GET, `${REQUEST_SHOPIFY_BASE_URL}products.json`);
    // console.log(allProducts);
    // const allProducts = sampleAllVariants;
    let strippedProducts = [];
    allProducts.products.forEach((prod) => {
      const { title } = prod;

      prod.variants.forEach((variant) => {
        const { sku } = variant;
        const { id: inventory_item_id } = variant;
        const stock = variant.inventory_quantity;

        // prod.title, prod['variants'].sku, prod['variants'].id, todo: what does source mean here?

        const obj = {
          shopifyId: inventory_item_id,
          title,
          sku,
          stock,
          source: '???',
        };

        strippedProducts.push(obj);

        // eslint-disable-next-line consistent-return
        (async () => { // async here because in forEach loop
          try {
            await VariantModel.updateOne(
              { shopifyId: inventory_item_id },
              { $set: obj },
              { upsert: true },
            );
          } catch (error) {
            return error;
          }
        })();
      });
    });
    // returns all products just added to mongodb
    return strippedProducts;
  } catch (e) {
    console.error(e.message);
  }
}

const processes = {
  pullShopifyProcess: 'pull shopify',
  queueDeleteAll: 'delete all variants',
  deleteById: 'delete variant by id',
  postVariant: 'post variant'
};

const queue = kue.createQueue({ redis: { CONNECTION } });
for (let process in processes) {
  queue.create(process, {})
    .removeOnComplete(true)
    .attempts(2)
    .backoff({
      delay: 80 * 10000,
      type: 'exponential'
    })
    .save();
}


// define what 'pull shopify' process does
const syncShopifyInfo = () => {
  queue.process(processes.pullShopifyProcess,
    async (job, done) => {
      getAllProducts()
        .then( // getting and parsing information from shopify -- try to figure out how to not reparse info for what already exists, but delete anything that no longer exists
          done()); // need
    });

};

let result;
const deleteAllVariants = async () => {
  await queue.process(processes.queueDeleteAll,  // process needs to be after create
    async (job, done) => {
      result = await VariantModel.deleteMany({ 'shopifyId': { $regex: '' } }) // TODO not tested
        .exec();
      done();
    });
  return result;
};

SERVER.route({
  method: 'GET',
  path: '/sync',
  handler: async (request, h) => {
    try {
      await syncShopifyInfo();
      return h.response(
        {
          statusCode: 200,
          message: 'successfully synced',
          json: true
        }
      );
    } catch (error) {
      console.error(error);
      return h.response(error)
        .code(500);
    }
  }
}); // tested returns code and message -- need to test database update and deletion with sync

SERVER.route({
  method: DELETE,
  path: '/allVariants',
  handler: async (request, h) => {
    // const res = await deleteAllVariants(); // todo not returning as expected
    // // if (result.deletedCount <= 0) throw new Error('Nothing to delete'); // TODO not tested
    // // return {
    // //   code: 200,
    // //   message: 'successfully deleted'
    // // };
    let result;
    await queue.process(processes.queueDeleteAll,  // process needs to be after create
      async (job, done) => {
        try {
          result = await VariantModel.deleteMany({ 'shopifyId': { $regex: '' } }) // TODO not tested
            .exec();
          done();
          console.log(h.response(result));
          return h.response(result);
        } catch (error) {
          done();
          return h.response(error.message) // TODO not tested
            .code(404);
        }
      }
    );
  } // tested
});

// Route for manually deleting an entry from the db
SERVER.route({
  method: DELETE,
  path: '/variant/{id}',
  handler: async (request, h) => {
    try {
      let result;
      // const result = await VariantModel.deleteOne({ _id: request.params.id }) // TODO not tested
      //   .exec();
      await queue.process(processes.deleteById,
        async (job, done) => {
          result = await VariantModel.deleteOne({ _id: request.params.id }) // TODO not tested
            .exec();
          done();
        });
      if (result.deletedCount <= 0) throw new Error('Object does not exist to delete'); // TODO not tested
      return h.response(result); // TODO not tested
    } catch (error) {
      return h.response(error.message) // TODO not tested
        .code(404);
    }
  },
  options: {
    validate: {
      params: {
        id: Joi.string()
          .length(24)
      }
    }
  }
}); // has test, does not pass


// Route for manually adding a varaint to the db
SERVER.route({
  method: POST,
  path: '/variant',
  options: {
    validate: {
      payload: {
        _id: Joi.string()
          .optional(),
        //   .required(), // object id has to have a length of 24 -- throws error if not
        title: Joi.string()
          .required(),
        sku: Joi.string()
          .required(),
        stock: Joi.number()
          .required(),
        shopifyId: Joi.string()
          .required(),
        source: Joi.string()
          .required(),
      },
    },
  },
  handler: async (request, h) => {
    queue.process(processes.deleteById, async (job, done) => {
      try {
        const variant = new VariantModel(request.payload);
        const result = await variant.save();
        done();
        return h.response(result)
          .code(201);
      } catch (error) {
        done();
        return h.response(error.details) // TODO not tested
          .code(409);
      }
    });
  },
}); // tested

module.exports = {
  syncShopifyInfo,
  getAllProducts,
  deleteAllVariants,
};
