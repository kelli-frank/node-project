const Joi = require('joi');
// const { deleteAllVariants } = require('./queuing');
const {
  VariantModel, SERVER, GET, POST, DELETE
} = require('./config.js');
// const { deleteAllVariants } = require('./queuing.js');
// eslint-disable-next-line no-unused-expressions

SERVER.route({
  method: GET,
  path: '/',
  handler: async (request, h) => h.response({
    statusCode: 200,
    message: 'Successfully connected'
  })
}); // tested

// Route for getting all variants from mongo
SERVER.route({
  method: GET,
  path: '/allVariants',
  handler: async (request, h) => {
    try {
      const allVariants = await VariantModel.find()
        .exec();
      return h.response(allVariants);
    } catch (error) {
      return h.response(error)
        .code(500);
    }
  },
}); // tested

// Route for getting single variants from mongo by SHOPIFY(?) id
async function getVariantParam() {
  return ({
    method: GET,
    path: '/variant/{id}',
    options: {
      // validate: {
      //   // params: {
      //   //   id: Joi.string()
      //   //     .required()
      //   // }
      // }
    },
    handler: async (request, h) => {
      let variant;
      try {
        variant = await VariantModel.find({ _id: request.params.id }) // TODO not tested
          .exec();
        if (variant.length <= 0) throw new Error('_id not found');
        return h.response(variant); // TODO not tested
      } catch (error) {
        throw new Error(error); // TODO not tested
      }
    },
  }); // has test, does not pass
}

(async () => {
  SERVER.route(await getVariantParam()
    .then(error => error)
    .catch((error) => {
      console.error(error); // TODO not tested
    }));
})();
// .then(async (object) => {
//   await console.log(object);
//   SERVER
//     .route(object) // error here
//     .catch((error) => {
//       console.error(error);
//       return error
//         .code(204);
//     });
// });
//
// // Route for manually adding a varaint to the db
// SERVER.route({
//   method: POST,
//   path: '/variant',
//   options: {
//     validate: {
//       payload: {
//         _id: Joi.string()
//           .optional(),
//         //   .required(), // object id has to have a length of 24 -- throws error if not
//         title: Joi.string()
//           .required(),
//         sku: Joi.string()
//           .required(),
//         stock: Joi.number()
//           .required(),
//         shopifyId: Joi.string()
//           .required(),
//         source: Joi.string()
//           .required(),
//       },
//     },
//   },
//   handler: async (request, h) => {
//     try {
//       const variant = new VariantModel(request.payload);
//       const result = await variant.save();
//       return h.response(result)
//         .code(201);
//     } catch (error) {
//       return h.response(error.details) // TODO not tested
//         .code(409);
//     }
//   },
// }); // tested

// // Route for manually deleting an entry from the db
// SERVER.route({
//   method: DELETE,
//   path: '/variant/{id}',
//   handler: async (request, h) => {
//     try {
//       const result = await VariantModel.deleteOne({ _id: request.params.id }) // TODO not tested
//         .exec();
//       if (result.deletedCount <= 0) throw new Error('Object does not exist to delete'); // TODO not tested
//       return h.response(result); // TODO not tested
//     } catch (error) {
//       return h.response(error.message) // TODO not tested
//         .code(404);
//     }
//   },
//   options: {
//     validate: {
//       params: {
//         id: Joi.string()
//           .length(24)
//       }
//     }
//   }
// }); // has test, does not pass

// const testingDeleteFunc = async () => {
// SERVER.route({
//   method: DELETE,
//   path: '/allVariants',
//   handler: async (request, h) => {
//     try {
//       await deleteAllVariants();
//       return h.response(result);
//     } catch (error) {
//       return h.response(error.message) // TODO not tested
//         .code(404);
//     }
//   }
// }); // tested
// };

// module.exports = { testingDeleteFunc };
