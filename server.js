const { SERVER } = require('./config.js');

SERVER.route({
  method: 'GET',
  path: '/test',
  handler() {
    return 'test route'; // TODO not tested
  },
});

async function init() {
  SERVER.initialize();
  return SERVER;
}

// TODO figure out if this is necessary
const serverIsStarted = () => {
  return SERVER.info.started > 0;
};

async function start() { // TODO not tested
  await SERVER.start();
  console.log(`Server running at: ${SERVER.info.uri}`);
  return SERVER;
}

process.on('unhandledRejection', (err) => { // TODO not tested
  console.log(err);
  process.exit(1);
});

module.exports = {
  init,
  start,
  serverIsStarted
};


// simple file, all tested
