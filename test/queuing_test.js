const nock = require('nock');
const queue = require('kue')
  .createQueue(); // tutorial has no const, let, var, etc
const { expect } = require('@hapi/code');
// eslint-disable-next-line no-multi-assign
const {
  it, describe, before, afterEach, after
// eslint-disable-next-line no-multi-assign
} = exports.lab = require('@hapi/lab')
  .script();
const { GET } = require('../config');
const { sampleAllVariants, sampleAllVariantsStripped } = require('../config');
const { pullShopifyInfo, getAllProducts } = require('../queuing.js');
const { init } = require('../server.js');

let server;
describe('Testing queuing.js', () => {
  before(async () => {
    server = await init();
    await queue.testMode.enter(true); // by default jobs aren't processed when created during test mode, need true to enable that
  });

  afterEach(() => {
    queue.testMode.clear();
  });

  after(() => {
    queue.testMode.exit();
  });

  it('testing out kue testing', () => {
    queue.createJob('job 1', { a: 'one' })
      .save();
    queue.createJob('job 2', { b: 'two' })
      .save();
    expect(queue.testMode.jobs.length)
      .to
      .equal(2);
    expect(queue.testMode.jobs[0].type)
      .to
      .equal('job 1');
    expect(queue.testMode.jobs[0].data)
      .to
      .equal({ a: 'one' });
  });

  // it('Test pullShopifyInfo() adds jobs to the queue', async () => {
  //   const numJobs = Math.random() * 100;
  //
  //   // eslint-disable-next-line no-plusplus
  //   for (let i = 0; i < 10; i++) {
  //     pullShopifyInfo();
  //   }
  //   const currQueue = await pullShopifyInfo();
  //   expect(currQueue.testMode.jobs.length === numJobs + 1);
  // });

  // async function syncProductsNock() {
  //   await nock(REQUEST_SHOPIFY_BASE_URL)
  //     // .persist()
  //     // .log(console.log)
  //     .get('/products.json')
  //     .reply(200, sampleAllVariants);
  // }

  it('Testing getAllProducts() -- nock for response', async () => {
    await nock('https://kelli-franks-store.myshopify.com')
    // .log(console.log)
      .get('/admin/api/2019-07/products.json')
      .reply(222, sampleAllVariants)
      .persist();

    // const res = await server.inject({
    //   method: GET,
    //   url: '/sync'
    // });
    //
    // it('Testing out nock', async () => {
    //   expect(res)
    //     .to
    //     .equal('TESTING');
    // });

    // const nocking = async () => {
    //   await nock('localhost')
    //     .get('/sync')
    //     .reply(201, sampleAllVariants);
    // };
    //
    // await nocking(); // start the nocking for /sync

    const res = await getAllProducts();

    await expect(res) // want getAllProducts() to correctly parse the data
      .to
      .equal(sampleAllVariantsStripped);
  });
});


