/* eslint-disable indent */
const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const nock = require('nock');
const crypto = require('crypto');
const {
  GET, POST, DELETE, sampleAllVariants, VariantModel
} = require('../config.js');
let { mongodbId, testPOSTBodyNo_id } = require('../config.js');
require('../queuing.js');
const { bodyWith_id } = require('../config');


const {
  afterEach, before, describe, it, after, beforeEach
// eslint-disable-next-line no-multi-assign
} = exports.lab = Lab.script();
const { init } = require('../server.js');

let server;

/* eslint-disable no-param-reassign */
async function getApiCallOptions(method, url, body) {
  body = body === undefined ? {} : body;
  return Object.keys(body).length === 0
    ? {
      method,
      url,
    } : {
      method,
      url,
      payload: body,
    };
}

async function testRouteStatusCode(code, method, url, body, notes) {
  body = body || {};
  notes = notes === undefined ? '' : ` --> (${notes})`;

  // generate a mongodbId if it hasn't happened yet
  if (mongodbId === undefined) {
    await getNewDbId(); // checks in get function
  }

  // edit url to contain the id instead of undefined
  const undefinedIdx = url.indexOf(undefined);
  if (undefinedIdx > -1) {
    url = url.substring(0, undefinedIdx) + mongodbId;
  }
  await it(` ${method} ${url}  responds with ${code}${notes}`, async () => {
    const inner = await (Object.keys(body).length <= 0 ? url : await getApiCallOptions(method, url, body));
    if (server === undefined) {
      server = await init();
    }
    const res = await server.inject(inner);

    await expect(res.statusCode)
      .to
      .equal(code);

    return res;
  });
}

// async function testRouteStatusCode(200,method, url, body) {
//   return testRouteStatusCode(200, method, url, body);
// }

// TODO: eventually refactor into different describes for each method?


async function getNewDbId() {
  if (mongodbId !== undefined) {
    return mongodbId;
  }
  let findId;
  do {
    // eslint-disable-next-line one-var
    mongodbId = crypto.randomBytes(12)
      .toString('hex');

    // const testId = '12345678912345678912345a';
    // eslint-disable-next-line no-await-in-loop
    findId = await VariantModel.findById(mongodbId);
  }
  while (findId !== null);

  return mongodbId;
}

describe('Testing routes', () => {
  // Gets an id to use for testing routes -- ensures that this id does not yet exist in the database

  beforeEach(async () => {
    server = await init();
    await getNewDbId();
  });

  after(async () => {
    await server.stop();
  });

  describe('Checking valid mongodbId', async () => {
    it('mongodbId is a valid hex value', () => expect(mongodbId.toString('hex') === mongodbId));
    it('mongodbId has a valid length of 24', () => expect(mongodbId.length === 24));
  });

  // checks default route exists
  testRouteStatusCode(200, GET, '/'); // running before 'before'

  // checks route deleting all returns 200
  testRouteStatusCode(200, DELETE, '/allVariants');

  // test posting with _id
  testRouteStatusCode(201, POST, '/variant',
    bodyWith_id, 'with _id');
  // .then(() => {
  // });

  // test posting without _id
  testRouteStatusCode(201, POST, '/variant',
    testPOSTBodyNo_id, 'without explicit _id field');

  // checks returning all variants is returning a 200 status code
  testRouteStatusCode(200, GET, '/allVariants');

  // test getting object just posted
  testRouteStatusCode(200, GET, `variant/${mongodbId}`); // todo: returning with 400 -- unsure why

  // it('testing getting variant/{id} with validation', async () => {
  //   const res = await server.inject(
  //     {
  //       method: GET,
  //       url: 'variant/{id}',
  //       headers: {
  //         'Accept-Encoding': 'gzip, deflate',
  //       }
  //     },
  //     'without explicit _id field'
  //   );
  //
  //   // has two res.result keys
  //   await expect(res.statusCode)
  //     .to
  //     .equal(200);
  //   return res;
  // });


// delete this object so it can be tested again
  testRouteStatusCode(200, DELETE, 'variant/' + mongodbId); // todo: returning with 400 -- unsure why

// checks GET /variant does not exist
  testRouteStatusCode(404, GET, '/variant'); // does not need await because it should return the same thing anytime

  it('Testing sync route', async () => {
    const res = await server.inject({
      method: GET,
      url: '/sync'
    });

    expect(res.statusCode)
      .to
      .equal(200);

    expect(res.result.message)
      .to
      .equal('successfully synced');
  });
});
