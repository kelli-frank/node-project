// const { agenda } = require('../scheduler.js');
const Lab = require('@hapi/lab');
const { TIME_INTERVAL, PROCESS_EVERY, bodyWith_id, REQUEST_SHOPIFY_BASE_URL, PUT } = require('../config');
const { afterEach, before, describe, it, after, beforeEach } = exports.lab = Lab.script();
const { expect } = require('@hapi/code');
const { init } = require('../server.js');
// describe('Testing scheduler.js file', async () => {
//   await sleep(300);
// });


// /sync updated the db correctly

describe('Testing scheduler.js', async () => {
  let server;
  before(async () => {
    server = await init();
  });
  it('Checking that changing Shopify changes what\'s in the db', async () => {
    let getting = await server.inject({
      method: 'GET',
      url: '/allVariants'
    });
    getting = JSON.parse(getting.payload);
    expect(getting !== undefined);

    let rand_inventory_item_id;
    do {
      rand_inventory_item_id = getting[Math.floor(Math.random() * Object.keys(getting).length)].shopifyId;
    } while (rand_inventory_item_id === '123');

    let shopRes = await (server.inject({ // TODO this needs to be an update to Shopify
      method: PUT,
      url: REQUEST_SHOPIFY_BASE_URL + 'inventory_items/#' + rand_inventory_item_id + '.json',
      payload: bodyWith_id
    }));

    let afterPost = await server.inject({
      method: 'GET',
      url: '/allVariants'
    });
    console.log('sleeping...');
    // await sleep(Math.max(TIME_INTERVAL, PROCESS_EVERY));

    expect(getting !== afterPost);
  });
});


function msleep(n) {
  Atomics.wait(new Int32Array(new SharedArrayBuffer(4)), 0, 0, n);
}

function sleep(n) {
  msleep(n * 1000);
}
