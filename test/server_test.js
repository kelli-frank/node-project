const { expect } = require('@hapi/code');
// eslint-disable-next-line no-multi-assign
const { it, describe, before } = exports.lab = require('@hapi/lab')
  .script();
const { init } = require('../server.js');

describe('Testing server.js', () => {
  let server;
  before(async () => {
    server = await init();
  });

  it('checking the server is initialized', () => {
    expect(server.info.created > 0);
  });
});
