const { expect } = require('@hapi/code');
// eslint-disable-next-line no-multi-assign
const { it, describe } = exports.lab = require('@hapi/lab')
  .script();
const { VariantModel } = require('../config.js');

describe('Checking VariantModel schema', () => {
// Test the Variant Model schema is correct
  it('VariantModel should have correct schema', async () => {
    expect(VariantModel.schema.obj)
      .to
      .equal(
        {
          shopifyId: String, // use Shopify id as db's _id field
          title: String,
          sku: String,
          stock: Number,
          source: String,
        }
      );
  });
});
